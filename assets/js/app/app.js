/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    angular.module('scool', [
        'ui.bootstrap',                 // Ui Bootstrap
        'datatables',
        'datePicker',
        'oitozero.ngSweetAlert',
        'summernote',
        'daterangepicker',
        'ui.calendar',

    ])
})();

