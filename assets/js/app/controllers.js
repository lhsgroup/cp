/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */
function addClassCtrl($scope, $modal) {

  $scope.addChild = function () {
    var modalInstance = $modal.open({
      templateUrl: '/templates/modals/addChild.html',
      controller: ModalInstanceCtrl
    });
  };
  $scope.addTeacher = function () {
    var modalInstance = $modal.open({
      templateUrl: '/templates/modals/addTeacher.html',
      controller: ModalInstanceCtrl
    });
  };
  $scope.addParent = function () {
    var modalInstance = $modal.open({
      templateUrl: '/templates/modals/addParent.html',
      controller: ModalInstanceCtrl
    });
  };
  $scope.sendMsgClass = function () {
    var modalInstance = $modal.open({
      templateUrl: '/templates/modals/sendMsgClass.html',
      controller: ModalInstanceCtrl
    });
  };

  $scope.summernoteText = ['<h3>Hello Jonathan! </h3>',
    '<p>dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the dustrys</strong> standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more',
    'recently with</p>'].join('');

  $scope.daterange = {startDate: null, endDate: null}


};

function ModalInstanceCtrl ($scope, $modalInstance) {

  $scope.ok = function () {
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

};

function sweetAlertCtrl($scope, SweetAlert) {

  $scope.alertDelete = function () {
    SweetAlert.swal({
        title: "Are you sure?",
        text: "Your will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function (isConfirm) {
        if (isConfirm) {
          SweetAlert.swal("Deleted!", "Your file has been deleted.", "success");
        } else {
          SweetAlert.swal("Cancelled","The file was not deleted!", "error");
        }
      });
  }

}

function CalendarCtrl($scope) {

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  // Events
  $scope.events = [
    {title: 'All Day Event',start: new Date(y, m, 1)},
    {title: 'Long Event',start: new Date(y, m, d - 5),end: new Date(y, m, d - 2)},
    {id: 999,title: 'Repeating Event',start: new Date(y, m, d - 3, 16, 0),allDay: false},
    {id: 999,title: 'Repeating Event',start: new Date(y, m, d + 4, 16, 0),allDay: false},
    {title: 'Birthday Party',start: new Date(y, m, d + 1, 19, 0),end: new Date(y, m, d + 1, 22, 30),allDay: false},
    {title: 'Click for Google',start: new Date(y, m, 28),end: new Date(y, m, 29),url: 'http://google.com/'}
  ];


  /* message on eventClick */
  $scope.alertOnEventClick = function( event, allDay, jsEvent, view ){
    $scope.alertMessage = (event.title + ': Clicked ');
  };
  /* message on Drop */
  $scope.alertOnDrop = function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view){
    $scope.alertMessage = (event.title +': Droped to make dayDelta ' + dayDelta);
  };
  /* message on Resize */
  $scope.alertOnResize = function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ){
    $scope.alertMessage = (event.title +': Resized to make dayDelta ' + minuteDelta);
  };

  /* config object */
  $scope.uiConfig = {
    calendar:{
      height: 450,
      editable: true,
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      eventClick: $scope.alertOnEventClick,
      eventDrop: $scope.alertOnDrop,
      eventResize: $scope.alertOnResize
    }
  };

  /* Event sources array */
  $scope.eventSources = [$scope.events];
}

angular
    .module('scool')
    .controller('addClassCtrl', addClassCtrl)
    .controller('sweetAlertCtrl', sweetAlertCtrl)
    .controller('CalendarCtrl', CalendarCtrl)
