/**
 * grunt/pipeline.js
 *
 * The order in which your css, javascript, and template files should be
 * compiled and linked from your views and static HTML files.
 *
 * (Note that you can take advantage of Grunt-style wildcard/glob/splat expressions
 * for matching multiple files.)
 *
 * For more information see:
 *   https://github.com/balderdashy/sails-docs/blob/master/anatomy/myApp/tasks/pipeline.js.md
 */


// CSS files to inject in order
//
// (if you're using LESS with the built-in default config, you'll want
//  to change `assets/styles/importer.less` instead.)
var cssFilesToInject = [
  'styles/**/*.css'
];


// Client-side javascript files to inject in order
// (uses Grunt-style wildcard/glob/splat expressions)
var jsFilesToInject = [

  // Load sails.io before everything else
  'js/dependencies/sails.io.js',

  // Dependencies like jQuery, or Angular are brought in here
  'js/dependencies/**/*.js',

  'js/vendor/jquery/jquery-2.1.1.min.js',
  'js/vendor/plugins/jquery-ui.min.js',
  'js/vendor/bootstrap/bootstrap.min.js',

  'js/vendor/plugins/jquery.metisMenu.js',
  'js/vendor/plugins/jquery.slimscroll.min.js',
  'js/vendor/plugins/pace.min.js',

  'js/vendor/angular/angular.min.js',
  'js/vendor/angular/angular-animate.min.js',
  'js/vendor/plugins/jquery.dataTables.js',
  'js/vendor/plugins/dataTables.bootstrap.js',
  'js/vendor/plugins/angular-datatables.min.js',
  'js/vendor/bootstrap/ui-bootstrap-tpls-0.12.0.min.js',
  'js/vendor/plugins/chosen.jquery.js',
  'js/vendor/plugins/angular-datepicker.js',
  'js/vendor/plugins/angular-sweetalert.min.js',
  'js/vendor/plugins/sweetalert.min.js',
  'js/vendor/plugins/summernote.min.js',
  'js/vendor/plugins/angular-summernote.min.js',
  'js/vendor/plugins/icheck.min.js',
  'js/vendor/plugins/moment.min.js',
  'js/vendor/plugins/daterangepicker.js',
  'js/vendor/plugins/angular-daterangepicker.js',
  'js/vendor/plugins/fullcalendar.min.js',
  'js/vendor/plugins/gcal.js',
  'js/vendor/plugins/calendar.js',

  'js/app/inspinia.js',
  'js/app/app.js',
  'js/app/config.js',
  'js/app/directives.js',
  'js/app/controllers.js'

];


// Client-side HTML templates are injected using the sources below
// The ordering of these templates shouldn't matter.
// (uses Grunt-style wildcard/glob/splat expressions)
//
// By default, Sails uses JST templates and precompiles them into
// functions for you.  If you want to use jade, handlebars, dust, etc.,
// with the linker, no problem-- you'll just want to make sure the precompiled
// templates get spit out to the same file.  Be sure and check out `tasks/README.md`
// for information on customizing and installing new tasks.
var templateFilesToInject = [
  'templates/**/*.html'
];







// Default path for public folder (see documentation for more information)
var tmpPath = '.tmp/public/';

// Prefix relative paths to source files so they point to the proper locations
// (i.e. where the other Grunt tasks spit them out, or in some cases, where
// they reside in the first place)
module.exports.cssFilesToInject = cssFilesToInject.map(function(cssPath) {
  return require('path').join('.tmp/public/', cssPath);
});
module.exports.jsFilesToInject = jsFilesToInject.map(function(jsPath) {
  return require('path').join('.tmp/public/', jsPath);
});
module.exports.templateFilesToInject = templateFilesToInject.map(function(tplPath) {
  return require('path').join('assets/',tplPath);
});


