/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // Login

  '/login': {
    view: 'users/login',
    locals:{
      layout:'layouts/login'
    }
  },

  // Register

  '/register': {
    view: 'users/register',
    locals:{
      layout:'layouts/login'
    }
  },

  // Forgot Password

  '/forgot-password': {
    view: 'users/forgotPassword',
    locals:{
      layout:'layouts/login'
    }
  },

  // Dashboard

  '/': {
    view: 'pages/dashboard',
    locals:{
      layout:'layouts/default'
    }
  },

  // Search Results

  '/search-results': {
    view: 'pages/searchResults',
    locals:{
      layout:'layouts/default'
    }
  },

  // Notification

  '/notification': {
    view: 'pages/notification',
    locals:{
      layout:'layouts/default'
    }
  },

  // Profile

  '/profile': {
    view: 'pages/profile',
    locals:{
      layout:'layouts/default'
    }
  },

  // Messages

  '/messages': {
    view: 'pages/messages',
    locals:{
      layout:'layouts/default'
    }
  },

  // Food Menu

  '/food-menu': {
    view: 'pages/foodMenu',
    locals:{
      layout:'layouts/default'
    }
  },


  // Class

  '/class': {
    view: 'class/index',
    locals:{
      layout:'layouts/default'
    }
  },
  '/class/edit': {
    view: 'class/edit',
    locals:{
      layout:'layouts/default'
    }
  },
  '/class/add': {
    view: 'class/add',
    locals:{
      layout:'layouts/default'
    }
  },

  // Teacher

  '/teacher': {
    view: 'teacher/index',
    locals:{
      layout:'layouts/default'
    }
  },
  '/teacher/edit': {
    view: 'teacher/edit',
    locals:{
      layout:'layouts/default'
    }
  },
  '/teacher/add': {
    view: 'teacher/add',
    locals:{
      layout:'layouts/default'
    }
  },

  // Parent

  '/parent': {
    view: 'parent/index',
    locals:{
      layout:'layouts/default'
    }
  },
  '/parent/edit': {
    view: 'parent/edit',
    locals:{
      layout:'layouts/default'
    }
  },
  '/parent/add': {
    view: 'parent/add',
    locals:{
      layout:'layouts/default'
    }
  },

  // Child

  '/child': {
    view: 'child/index',
    locals:{
      layout:'layouts/default'
    }
  },
  '/child/edit': {
    view: 'child/edit',
    locals:{
      layout:'layouts/default'
    }
  },
  '/child/add': {
    view: 'child/add',
    locals:{
      layout:'layouts/default'
    }
  },

  // Events

  '/events': {
    view: 'events/index',
    locals:{
      layout:'layouts/default'
    }
  },
  '/events/edit': {
    view: 'events/edit',
    locals:{
      layout:'layouts/default'
    }
  },
  '/events/add': {
    view: 'events/add',
    locals:{
      layout:'layouts/default'
    }
  }




  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
